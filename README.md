HEAT
====================


Quality monitoring
------------------

### GitLab badges
[![pipeline status](https://gitlab.inria.fr/sed-bso/heat/badges/master/pipeline.svg)](https://gitlab.inria.fr/sed-bso/heat/commits/master)
[![coverage report](https://gitlab.inria.fr/sed-bso/heat/badges/master/coverage.svg)](https://gitlab.inria.fr/sed-bso/heat/commits/master)

### Coverity badge
[![coverity status](https://scan.coverity.com/projects/19229/badge.svg)](https://scan.coverity.com/projects/heat)

### SonarQube badges
[![Lines of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=ncloc)
[![Comment line density](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=comment_lines_density)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=coverage)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=coverage)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=bugs)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=bugs)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=vulnerabilities)
[![Code Smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=code_smells)

[![New Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_bugs)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_bugs)
[![New Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_vulnerabilities)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_vulnerabilities)
[![New Code Smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_code_smells)](https://sonarqube.inria.fr/sonarqube/component_measures?id=sedbso%3Aheat%3Agitlab%3Amaster&metric=new_code_smells)

Mathematical problem
---------------------

This C program aims at solving the following **heat propagation** equation

```math
\frac{\partial u(x,t) }{\partial t} - \Delta u(x,t) = 0 \qquad \forall  t \in [0,T] \, , \forall x \in [0,1]^2
```
```math
u(x,t) = 1 \, \qquad \forall  t \in [0,T] \, , \forall x \in \partial [0,1]^2.
```

Project
---------------------

This program serves as a toy code.
Several software engineering techniques are used:

* CMake build system with CTest
* Doxygen documentation
* A pipeline to test the code, either gitlab-ci (.gitlab-ci.yml) or Jenkins (Jenkinsfile) can be used
* Org-mode script for the code analysis and the integration into a SonarQube instance

Formation
---------------------

* [Gitlab-CI](https://sed-bso.gitlabpages.inria.fr/gitlab-ci/)
* [SonarQube](https://sed-bso.gitlabpages.inria.fr/sonarqube/)